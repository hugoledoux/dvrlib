##### Hello World with OpenMP

Here is an OpenMP-version `hello-mp.cpp` of Hello World:

```
/******************************************************************************
*   OpenMP Example - Hello World - C/C++ Version
*   In this simple example, the master thread forks a parallel region.
*   All threads in the team obtain their unique thread number and print it.
*   The master thread only prints the total number of threads.  Two OpenMP
*   library routines are used to obtain the number of threads and each
*   thread's number.
* Adapted from: https://computing.llnl.gov/tutorials/openMP/samples/C/omp_hello.c
******************************************************************************/
#include <omp.h>
#include <iostream>

int main ()
{
int nthreads, tid;

// Fork a team of threads giving them their own copies of variables
#pragma omp parallel private(nthreads, tid)
  {

	// Obtain thread number 
  tid = omp_get_thread_num();
  std::cout<<"\nHello World from thread = "<<tid<<"\n";

  // Only master thread does this
  if (tid == 0) 
    {
    nthreads = omp_get_num_threads();
    std::cout<<"\nNumber of threads = "<<nthreads<<"\n";
    }

  }  // All threads join master thread and disband

}
```

Successful execution should print the number of threads once and a
Hello World message from each thread in a (very) jumbled manner.

**Compile and run from the terminal** with `-fopenmp` and `CXX` set to
  `g++` or `clang++` (for example)  

```
CXX hello-mp.cpp -o hello -fopenmp
./hello
```

* To generate a makefile with CMake, use the`CMakeLists.txt` file:   
```
# Version of cmake
cmake_minimum_required(VERSION 3.10.0)

# Find OpenMP
find_package(OpenMP)
if(NOT OpenMP_CXX_FOUND)
 message(FATAL_ERROR
				"Could not locate OpenMP. Try adding
				-DCMAKE_CXX_FLAGS=-fopemp" )
endif()


# Add this target
add_executable(hello hello-mp.cpp)

# Link OpenMP libraries
target_link_libraries(hello PUBLIC OpenMP::OpenMP_CXX)

```

Generate a makefile:
```
mkdir build
cd build
cmake ../ -DCMAKE_CXX_COMPILER=g++
make
./hello
```

* Could not find `<omp.h>`? Likely reason: `-fopenmp` flag is ignored
  by the compiler.




