
/** \file dvr-tetrahedra.cpp
 * \brief Tutorial-style example explaining the use of dvr to sequentially optimize vertices in a tet mesh.
 * \author Ramsharan Rangarajan
 * \date March 23, 2020
 * \example dvr-tetrahedra.cpp
 * This is an example demonstrating the use of the \b dvr library to 
 * relax a mesh of 3D tetrahedra.
 * 
 * The example proceeds as follows:
 * 
 * - Read a file called "P.mesh" in tecplot format
 * to a mesh object of type SimpleMesh
 * 
 * - Set the list of nodes to be relaxed, labeled \f${\rm I}_{\rm R}\f$
 * to be the set of all interior nodes, i.e., all nodes not lying on the 
 * boundary of the mesh
 * 
 * - Create a triangle mesh quality evaluator of type dvr::GeomTet3DQuality
 * 
 * - Create a max-min solver of type dvr::ReconstructiveMaxMinSolver. 
 * The dvr::PartitionedMaxMinSolver can be used as well, but is expected to be far less efficient.
 *
 * - Create a generator for random relaxation directions of type
 *   dvr::RandomDirGenerator.
 * 
 * - Iteratively class the function dvr::Optimize to sequentially relaxed
 * vertex positions in \f${\rm I}_{\rm R}\f$.
 * 
 * - The example prints a "mesh quality vector" at the end of each iteration
 * to a file. These files can be used to inspect the magnitude of improvement in 
 * element qualities, to monitor convergence of mesh qualities and hence to determine
 * a suitable number of relaxation iterations
 *
 * - The example prints the mesh realized at the end of each iteration 
 * in Tecplot format for the sake of visualization.
 * Notice that meshes differ from each other only in 
 * the locations of vertices in the list \f${\rm I}_{\rm R}\f$.
 * 
 * Running this example: 
 * - Create a build directory (any location)
 *   \code{.sh}
 *   mkdir build; 
 *   cd build
 *   \endcode
 * 
 * - Generate a <tt>makefile</tt> using <tt>cmake</tt>
 *   \code{.sh}
 *   cmake $DVRDIR/tuorial/
 *   make dvr-tetrahedra
 *   \endcode
 * 
 * - Alternately, 
 *   \code{.sh}
 *   CXX -std=c++11 -O2 -fopenmp -std=c++11 -I $DVRDIR/ -I $GSLPATH/include/  $DVRDIR/tutorial/dvr-tetrahedra.cpp -o dvr-tetrahedra -L $GSLPATH/lib/ -lgsl -lgslcblas </tt>
 *   \endcode
 *
 * - Execute: <tt>./dvr-tetrahedra </tt>
 * 
 * ----
 */

// Include all header files relevant to improving a tetrahedra mesh
#include <dvr_Module>

// Mesh data structure. Not part of the library
#include <mesh/dvrmesh_SimpleMesh.h>
#include <cstdlib>

// Print the quality vector measuring mesh quality
template<class QType>
void PrintMeshQuality(const std::string& filename,
		      const std::vector<int>& Ir,
		      const QType& Quality);

// Only class and functions with the dvr:: namespace are part
// of the library; rest are provided for demonstration and testing purposes

int main()
{
  const std::string dvrdir = std::getenv("DVRDIR");
  assert(dvrdir!="");
	 
  // Read a mesh in tecplot format
  // The "SimpleMesh" class and related utilities for reading and writing
  // meshes is provided along with this tutorial folder for convenience
  // and for the purpose of testing.
  // See the documentation for assumptions on the mesh data structure
  //! [doc_read_mesh]
  dvrmesh::SimpleMesh MD;
  MD.ReadTecplotFile(dvrdir+"/mesh/P.mesh");
  MD.SetupMesh();
  //! [doc_read_mesh]

  // Choose nodes to relax: Ir
  // In this example, we relax all nodes except the ones lying
  // on the boundary of the mesh.
  // These nodes are identified as the complement of the set of boundary nodes
  const auto bdnodes = MD.GetBoundaryNodes();
  std::vector<int> Ir{};
  std::set<int> allnodes(MD.Connectivity.begin(), MD.Connectivity.end());
  std::set_difference(allnodes.begin(), allnodes.end(), bdnodes.begin(), bdnodes.end(),
		      std::inserter(Ir, Ir.begin()));

  // All mesh related data has been setup. DVR functionalities from here on.

  // Object to evaluate element qualities
  // Notice templating by mesh type
  //! [doc_quality]
  dvr::GeomTet3DQuality<decltype(MD)> Quality(MD);
  //! [doc_quality]

  // Create max-min solver
  //! [doc_solver]
  dvr::ReconstructiveMaxMinSolver<decltype(Quality)> solver(Quality);
  //! [doc_solver]
  
  // Relaxation direction generator
  //! [doc_rdir]
  dvr::RandomDirGenerator<3> dirGen; // Generate random directions for each vertex at each iteration
  //! [doc_rdir]
  
  // Print the mesh quality before relaxation
  PrintMeshQuality("q-pre.dat", Ir, Quality);

  // Iteratively relax
  const int Nr = 10; // Number of iterations
  for(int iter=0; iter<Nr; ++iter)
    {
      std::cout<<"\nIteration #"<<iter<<std::flush;
      
      // One iteration of relaxation along random directions
      dvr::Optimize(Ir, MD, solver, dirGen);

      // Optional: print the mesh and its quality after each iteration
      MD.PlotTecMesh("MD-"+std::to_string(iter)+".tec");
      PrintMeshQuality("q-"+std::to_string(iter)+".dat", Ir, Quality);
    }

    // Print the final relaxed mesh and its quality
  MD.PlotTecMesh("relaxed_mesh.tec");
  PrintMeshQuality("q-post.dat", Ir, Quality);

  std::cout<<"\n---done---\n"<<std::flush;
  // done
}

// Print the quality vector measuring mesh quality
template<class QType>
void PrintMeshQuality(const std::string& filename,
		      const std::vector<int>& Ir,
		      const QType& Quality)
{
  // Sorted vector of qualities at vertices
  const int nVerts = static_cast<int>(Ir.size());
  std::vector<double> qvec(nVerts);
  for(int i=0; i<nVerts; ++i)
    qvec[i] = dvr::ComputeVertexQuality(Ir[i], Quality);
  std::sort(qvec.begin(), qvec.end());

  // Print to file
  std::fstream qfile;
  qfile.open(filename.c_str(), std::ios::out);
  assert(qfile.good());
  qfile<<"# Index \t Vertex quality";
  for(int i=0; i<nVerts; ++i)
    qfile<<"\n"<<i+1<<" "<<qvec[i];  
  qfile.flush(); qfile.close();
  return;
}
