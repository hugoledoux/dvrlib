
/** \file dvr-omp-triangle.cpp
 * \brief Tutorial-style example explaining the use of dvr to concurrently optimize vertices in a triangle mesh.
 * \author Ramsharan Rangarajan
 * \date March 23, 2020
 *
 * \example dvr-omp-triangle.cpp
 * This is an example demonstrating the 
 * the relaxation of a mesh of 
 * planar triangles using a thread-parallel implementation
 * of the \b dvr algorithm.
 *
 * The example proceeds as follows:
 *
 * - Read a file called "superior.mesh" in tecplot format
 * to a mesh object of type SimpleMesh
 * 
 * - Set the list of nodes to be relaxed, labeled \f${\rm I}_{\rm R}\f$
 * to be the set of all interior nodes, i.e., all nodes not lying on the 
 * boundary of the mesh
 * 
 * - Compute a vertex coloring for nodes in the list \f${\rm I}_{\rm R}\f$.
 * Vertices of the same color are relaxed concurrently.
 *
 * - Create a triangle mesh quality evaluator of type dvr::GeomTri2DQuality.
 * Specify the number of threads to use.
 * 
 * - Create a max-min solver of type dvr::ReconstructiveMaxMinSolver
 * The dvr::PartitionedMaxMinSolver can be used as well. The performance of the two solvers
 * is not drastically different when relaxing 2D triangle meshes, although the latter is expected
 * to be more efficient in general.
 *
 * - Create a relaxation direction generator of type dvr::RelaxationDirGenerator
 * A pre-implemented function called dvr::CartesianDirections templated by the 
 * spatial dimension is used to alternate between Cartesian directions over 
 * successive iterations. 
 * 
 * - Iteratively call the function dvr::Optimize to relaxed
 * vertex positions in \f${\rm I}_{\rm R}\f$. At each iteration, the 
 * routine concurrently relaxes all vertices of the same color.
 * 
 * - The example times the Optimize routine.
 * 
 * - The example prints a "mesh quality vector" at the end of each iteration
 * to a file. These files can be used to inspect the magnitude of improvement in 
 * element qualities, to monitor convergence of mesh qualities and hence to determine
 * a suitable number of relaxation iterations
 *
 * - The example prints the mesh realized at the end of each iteration 
 * in Tecplot format for the sake of visualization.
 * Notice that meshes differ from each other only in 
 * the locations of vertices in the list \f${\rm I}_{\rm R}\f$.
 * 
 * Running this example: 
 * - Create a build directory (any location)
 *   \code{.sh}
 *   mkdir build; 
 *   cd build
 *   \endcode
 * 
 * - Generate a <tt>makefile</tt> using <tt>cmake</tt>
 *   \code{.sh}
 *   cmake $DVRDIR/tuorial/
 *   make dvr-omp-triangle
 *   \endcode
 * 
 * - Alternately, 
 *   \code{.sh}
 *   CXX -std=c++11 -O2 -fopenmp -std=c++11 -I $DVRDIR/ -I $GSLPATH/include/  $DVRDIR/tutorial/dvr-omp-triangle.cpp -o dvr-omp-triangle -L $GSLPATH/lib/ -lgsl -lgslcblas </tt>
 *   \endcode
 *
 * - Execute: <tt>./dvr-omp-triangle </tt>
 * 
 * ----
 */

// Include all dvr header files
#include <dvr_Module>

// Mesh data structure. Not part of the library
#include <mesh/dvrmesh_SimpleMesh.h>
#include <cstdlib>
// Timing
#include <ctime>
#include <chrono>

// Print the quality vector measuring mesh quality
template<class QType>
void PrintMeshQuality(const std::string& filename,
		      const std::vector<int>& Ir,
		      const QType& Quality);


// Only class and functions with the dvr:: namespace are part
// of the library; rest are provided for demonstration and testing purposes

int main()
{
  const std::string dvrdir = std::getenv("DVRDIR");
  assert(dvrdir!="");
  
  // Read a mesh in tecplot format
  // The "SimpleMesh" class and related utilities for reading and writing
  // meshes is provided along with this tutorial folder for convenience
  // and for the purpose of testing.
  // See the documentation for assumptions on the mesh data structure
  dvrmesh::SimpleMesh MD;
  MD.ReadTecplotFile(dvrdir+"/mesh/superior.mesh");
  MD.SetupMesh();
  
  // Choose nodes to relax: Ir
  // In this example, we relax all nodes except the ones lying
  // on the boundary of the mesh.
  // These nodes are identified as the complement of the set of boundary nodes
  const auto bdnodes = MD.GetBoundaryNodes();
  std::vector<int> Ir{};
  std::set<int> allnodes(MD.Connectivity.begin(), MD.Connectivity.end());
  std::set_difference(allnodes.begin(), allnodes.end(), bdnodes.begin(), bdnodes.end(),
		      std::inserter(Ir, Ir.begin()));

  // All mesh related data has been setup. DVR functionalities from here on.

  // Prepare for thread-parallel execution.
  
  // Color the vertex list in Ir
  //! [doc_coloring]
  dvr::VertexColoring<decltype(MD)> vshades(MD, Ir);
  //! [doc_coloring]

  // Set max number of threads
  //! [doc_num_threads]
  const int nThreads = 3;
  //! [doc_num_threads]
  
  // Object to evaluate element qualities
  // Specify the number of threads
  //! [doc_quality]
  dvr::GeomTri2DQuality<decltype(MD)> Quality(MD, nThreads);
  //! [doc_quality]

  // Create max-min solver
  //! [doc_solver]
  dvr::ReconstructiveMaxMinSolver<decltype(Quality)> solver(Quality, nThreads);
  //! [doc_solver]
  
  // Relaxation direction generator
  dvr::CartesianDirGenerator<2> dirGen;

  // Print the mesh quality before relaxation
  PrintMeshQuality("q-pre.dat", Ir, Quality);
  
  //! [doc_omp_threads]
  omp_set_num_threads(nThreads);
  //! [doc_omp_threads]

    // Iteratively relax
  const int Nr = 10; // Number of iterations
  for(int iter=0; iter<Nr; ++iter)
    {
      std::cout<<"\nIteration #"<<iter<<std::flush;
      
      // Alternate between Cartesian directions with each iteration
      dirGen.iteration = iter;

      //! [doc_iteration]
      // Start timer
      double tstart = omp_get_wtime();
      Optimize(Ir, MD, solver, dirGen, vshades);
      double dt = omp_get_wtime()-tstart;
      //! [doc_iteration]

      std::cout<<": dt = "<<dt<<std::flush;
      
      // Optional: print the mesh and its quality after each iteration
      MD.PlotTecMesh("MD-"+std::to_string(iter)+".tec");
      PrintMeshQuality("q-"+std::to_string(iter)+".dat", Ir, Quality);
    }
  
  // Print the final relaxed mesh and its quality
  MD.PlotTecMesh("relaxed_mesh.tec");
  PrintMeshQuality("q-post.dat", Ir, Quality);

  std::cout<<"\n---done---\n"<<std::flush;
  // done
}

// Print the quality vector measuring mesh quality
template<class QType>
void PrintMeshQuality(const std::string& filename,
		      const std::vector<int>& Ir,
		      const QType& Quality)
{
  // Sorted vector of qualities at vertices
  const int nVerts = static_cast<int>(Ir.size());
  std::vector<double> qvec(nVerts);
  for(int i=0; i<nVerts; ++i)
    qvec[i] = dvr::ComputeVertexQuality(Ir[i], Quality);
  std::sort(qvec.begin(), qvec.end());

  // Print to file
  std::fstream qfile;
  qfile.open(filename.c_str(), std::ios::out);
  assert(qfile.good());
  qfile<<"# Index \t Vertex quality";
  for(int i=0; i<nVerts; ++i)
    qfile<<"\n"<<i+1<<" "<<qvec[i];  
  qfile.flush(); qfile.close();
  return;
}
