
/** \file dvr-cmd.cpp
 * \brief Tutorial-style example demonstrating using dvr with command line options.
 * \author Ramsharan Rangarajan
 * \date April 07, 2020
 *
 * \example dvr-cmd.cpp
 * An example to run \b dvr from the command line using a sequence
 * of arguments to relax either triangle or tetrahedral meshes.
 * The input and output file names, and the number of 
 * relaxation iterations are provided through commandline arguments.
 *
 * Assumes the following:
 * - Input file in tecplot format
 * - Output file in tecplot format
 * - All interior nodes are relaxed
 * - Relaxation is along Cartesian or randomly generated directions
 * during success iterations
 *
 * The TCLAP library is used to create and parse command line switches.
 *
 * Use the following switches:
 * - -d or \--details
 *    (optional) print files at each iteration, defaulted to false
 * - -c or --cartesian
 *     (optional) relax along Cartesian directions if specified, otherwise along random directions
 *
 * - -n (integer), \--nR (integer)
 *   (required)  # relaxation iterations
 *
 * - -o (string),  \--out (string)
 *   (required)  output tec filename
 *
 * - -i (string),  \--in (string)
 *   (required)  input tec filename
 *
 * - -j (integer), \--nthreads (integer)
 *   (optional) number of threads, defaulted to 1
 *
 * Running this example: 
 * - Create a build directory (any location)
 *   \code{.sh}
 *   mkdir build; 
 *   cd build
 *   \endcode
 * 
 * - Generate a <tt>makefile</tt> using <tt>cmake</tt>
 *   \code{.sh}
 *   cmake $DVRDIR/tuorial/
 *   make dvr-cmd
 *   \endcode
 * 
 * - Alternately, 
 *   \code{.sh}
 *   CXX -std=c++11 -O2 -fopenmp -std=c++11 -I $DVRDIR/ -I $GSLPATH/include/  $DVRDIR/tutorial/dvr-cmd.cpp -o dvr-cmd -L $GSLPATH/lib/ -lgsl -lgslcblas </tt>
 *   \endcode
 *
 * - Execute: <tt>./dvr-cmd -i input-file.tec -o output-file.tec -n num-relax-iters -j num-threads </tt>
 * 
 * ----
 */

// DVR stuff
#include <dvr_Module>

// SimpleMesh data structure
#include <mesh/dvrmesh_SimpleMesh.h>

// TCLAP for reading commandline arguments
#include <tclap/CmdLine.h>

// Read command line arguments using TCLAP
void ReadCommandLine(int argc, char** argv,
		     std::string& infile, std::string& outfile,
		     int& nR, int& nThreads, bool& flag_cartesian,
		     bool& flag_details);

// Print the quality vector measuring mesh quality
template<class QType>
void PrintMeshQuality(const std::string& filename,
		      const std::vector<int>& Ir,
		      const QType& Quality);

// Sequentially optimize a mesh of triangles or tetrahedra
template<class QType, const int SPD>
void SeqOptimizeMesh(dvrmesh::SimpleMesh& MD, const std::vector<int>& Ir,
		     const std::string& outfile, const int nR,
		     const bool flag_cartesian, const bool flag_details);

// Parallel optimizatione a mesh of triangles or tetrahedra
template<class QType, const int SPD>
void ompOptimizeMesh(dvrmesh::SimpleMesh& MD, const std::vector<int>& Ir,
		     const std::string& outfile, const int nR,
		     const int nThreads,
		     const bool flag_cartesian, const bool flag_details);


int main(int argc, char** argv)
{
  // Read command line arguments
  std::string infile, outfile;
  int nR, nThreads;
  bool flag_cartesian, flag_details;
  ReadCommandLine(argc, argv, infile, outfile, nR, nThreads, flag_cartesian, flag_details);
  
  // Read the mesh
  dvrmesh::SimpleMesh MD;
  MD.ReadTecplotFile(infile);
  MD.SetupMesh();

  // Nodes to relax
  const auto bdnodes = MD.GetBoundaryNodes();
  std::vector<int> Ir{};
  std::set<int> allnodes(MD.Connectivity.begin(), MD.Connectivity.end());
  std::set_difference(allnodes.begin(), allnodes.end(), bdnodes.begin(), bdnodes.end(),
		      std::inserter(Ir, Ir.begin()));
  std::cout<<"\n\nNumber of relaxed nodes: "<<Ir.size()<<std::flush;
  switch(nThreads)
    {
    case 1:   // sequential
      {
	if(MD.nodes_element==3) // Triangles
	  SeqOptimizeMesh<dvr::GeomTri2DQuality<decltype(MD)>,2>(MD, Ir, outfile, nR, flag_cartesian, flag_details);
	else // Tets
	  SeqOptimizeMesh<dvr::GeomTet3DQuality<decltype(MD)>,3>(MD, Ir, outfile, nR, flag_cartesian, flag_details);
	break;
      }
      
    default: // parallel?
      {
	if(MD.nodes_element==3) // Triangles
	  ompOptimizeMesh<dvr::GeomTri2DQuality<decltype(MD)>,2>(MD, Ir, outfile, nR, nThreads, flag_cartesian, flag_details);
	else // Tets
	  ompOptimizeMesh<dvr::GeomTet3DQuality<decltype(MD)>,3>(MD, Ir, outfile, nR, nThreads, flag_cartesian, flag_details);
      }
    }
  
  std::cout<<"\n---done---\n"<<std::flush;
}

// Sequentially optimize a mesh of triangles or tetrahedra
template<class QType, const int SPD>
void SeqOptimizeMesh(dvrmesh::SimpleMesh& MD, const std::vector<int>& Ir,
		     const std::string& outfile, const int nR,
		     const bool flag_cartesian, const bool flag_details)
{
  std::cout<<"\n\nExecuting sequential algorithm. "<<std::flush;
  
  // Mesh quality
  QType Quality(MD);

  // Solver
  dvr::ReconstructiveMaxMinSolver<decltype(Quality)> solver(Quality);

  // Relaxation directions
  dvr::RelaxationDirGenerator* dirGen;
  if(flag_cartesian==true)
    dirGen = new dvr::CartesianDirGenerator<SPD>;
  else
    dirGen = new dvr::RandomDirGenerator<SPD>;
  
  // Initial quality
  PrintMeshQuality("q-pre.dat", Ir, Quality);

  // Iteratively relax
  std::vector<double> dt(nR);
  for(int iter=1; iter<=nR; ++iter)
    {
      std::cout<<"\n\nIteration #"<<iter<<std::flush;

      if(flag_cartesian) dirGen->params = &iter;
      
      double tstart = omp_get_wtime();
      dvr::Optimize(Ir, MD, solver, *dirGen);
      dt[iter-1] = omp_get_wtime()-tstart;

      if(flag_details)
	{ //MD.PlotTecMesh("MD-"+std::to_string(iter)+".tec");
	  PrintMeshQuality("q-"+std::to_string(iter)+".dat", Ir, Quality);
	  std::cout<<", dt = "<<dt[iter-1]<<std::flush; }
    }
  
  // Final mesh and its quality
  MD.PlotTecMesh(outfile);
  PrintMeshQuality("q-post.dat", Ir, Quality);

  // Print average time per iteration
  double tavg = 0.;
  for(auto& t:dt) tavg += t;
  tavg /= static_cast<double>(nR);
  std::cout<<"\n\nAverage time per iteration: "<<tavg<<"\n"<<std::flush;

  delete dirGen;
  
  // -- done --
  return;
}

// Optimize a mesh of triangles or tetrahedra in parallel
template<class QType, const int SPD>
void ompOptimizeMesh(dvrmesh::SimpleMesh& MD, const std::vector<int>& Ir,
		     const std::string& outfile, const int nR,
		     const int nThreads,
		     const bool flag_cartesian, const bool flag_details)
{
  std::cout<<"\n\nExecuting parallel algorithm. "<<std::flush;
    
  // Mesh quality
  QType Quality(MD, nThreads);

  // Solver
  dvr::ReconstructiveMaxMinSolver<decltype(Quality)> solver(Quality, nThreads);

  // Vertex coloring
  dvr::VertexColoring<dvrmesh::SimpleMesh> vshades(MD, Ir);
  
  // Set the number of threads
#ifndef _OPENMP
  assert(false && "openMP not available");
#endif
  const int nMaxThreads = omp_get_max_threads()/2;
  assert(nThreads<=nMaxThreads && "Exceeded maximum number of threads");
  omp_set_num_threads(nThreads);
  
  // Relaxation directions
  dvr::RelaxationDirGenerator* dirGen;
  if(flag_cartesian==true)
    dirGen = new dvr::CartesianDirGenerator<SPD>;
  else
    dirGen = new dvr::RandomDirGenerator<SPD>(nThreads);
  
  // Initial quality
  PrintMeshQuality("q-pre.dat", Ir, Quality);

  // Iteratively relax
  std::vector<double> dt(nR);
  for(int iter=1; iter<=nR; ++iter)
    { std::cout<<"\n\nIteration #"<<iter<<std::flush;

      if(flag_cartesian)
	dirGen->params = &iter;
      
      double tstart = omp_get_wtime();
      dvr::Optimize(Ir, MD, solver, *dirGen, vshades);
      dt[iter-1] = omp_get_wtime()-tstart;

      if(flag_details)
	{ //MD.PlotTecMesh("MD-"+std::to_string(iter)+".tec");
	  PrintMeshQuality("q-"+std::to_string(iter)+".dat", Ir, Quality);
	  std::cout<<": dt = "<<dt[iter-1]<<std::flush; }
    } 

  // Print average time per iteration
  double tavg = 0.;
  for(auto& t:dt) tavg += t;
  tavg /= static_cast<double>(nR);
  std::cout<<"\n\nAverage time per iteration: "<<tavg<<"\n"<<std::flush;
  
  // Final mesh and its quality
  MD.PlotTecMesh(outfile);
  PrintMeshQuality("q-post.dat", Ir, Quality);

  delete dirGen;
  // done
}



// Print the quality vector measuring mesh quality
template<class QType>
void PrintMeshQuality(const std::string& filename,
		      const std::vector<int>& Ir,
		      const QType& Quality)
{
  // Sorted vector of qualities at vertices
  const int nVerts = static_cast<int>(Ir.size());
  std::vector<double> qvec(nVerts);
  for(int i=0; i<nVerts; ++i)
    qvec[i] = dvr::ComputeVertexQuality(Ir[i], Quality);
  std::sort(qvec.begin(), qvec.end());

  // Write the minimum quality on screen
  std::cout<<"\nMinimum quality: "<<qvec[0]<<std::flush;
  
  // Print to file
  std::fstream qfile;
  qfile.open(filename.c_str(), std::ios::out);
  assert(qfile.good());
  qfile<<"# Index \t Vertex quality";
  for(int i=0; i<nVerts; ++i)
    qfile<<"\n"<<i+1<<" "<<qvec[i];  
  qfile.flush(); qfile.close();
  return;
}


// Read command line arguments
void ReadCommandLine(int argc, char** argv,
		     std::string& infile, std::string& outfile,
		     int& nR, int& nThreads, bool& flag_cartesian,
		     bool& flag_details)
{
  // Setup commandline options
  TCLAP::CmdLine cmd("A utility for mesh optimization from the command line");
  
  // short name, long name, description, required?, default value, value-type description 
  // Input filename
  TCLAP::ValueArg<std::string> infileArg("i","in","input tec filename", true, "", "string");
  cmd.add(infileArg);

  // Output filename
  TCLAP::ValueArg<std::string> outfileArg("o", "out", "output tec filename", true, "", "string");
  cmd.add(outfileArg);

  // Number of relaxation iterations
  TCLAP::ValueArg<unsigned int> nRarg("n", "nR", "# relaxation iterations", true, 0, "integer");
  cmd.add(nRarg);

  // Number of threads
  TCLAP::ValueArg<unsigned int> jArg("j", "nthreads", "# threads", false, 1, "integer");
  cmd.add(jArg);

  // Relax along Cartesian directions
  TCLAP::SwitchArg dirArg("c", "cartesian", "relax along Cartesian directions", false);
  cmd.add(dirArg);
    
  // Print details during iterations?
  TCLAP::SwitchArg dArg("d", "details", "print mesh and qualities at each iteration?", false);
  cmd.add(dArg);

  // Parse command line switches
  cmd.parse( argc, argv );
  
  // Get values
  infile = infileArg.getValue();
  outfile = outfileArg.getValue();
  nR = nRarg.getValue();
  flag_details = dArg.getValue();
  flag_cartesian = dirArg.getValue();
  nThreads = jArg.getValue();

  // Print options
  std::cout<<"\nMesh improvement options"
	   <<"\n========================"
	   <<"\nInput file: "<<infile
	   <<"\nOutput file: "<<outfile
	   <<"\n# relaxation iterations: "<<nR
	   <<"\nRelaxation directions: "<< (flag_cartesian? "Cartesian" : "Random")
	   <<"\n# threads: "<<nThreads
	   <<"\nPrint files at each iteration: "<<(flag_details? "true" : "false")
	   <<"\n"<<std::flush;
  // -- done --
  return;
}
 
