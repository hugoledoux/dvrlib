
/** \file dvr_assert.h
 * \brief File defining the macro dvr::dvr_assert for data-dependent assertions.
 * \author Ramsharan Rangarajan
 * \date March 30, 2020
 */

#ifndef DVR_ASSERT_H
#define DVR_ASSERT_H

#include <cassert>
#include <iostream>

//! Namespace for all functionalties inherent to DVRlib.
namespace dvr
{
#define STR(x) #x
#define dvr_assert(x) if (!(x)) { std::cerr<<"DVR assertion failed: "<<STR(x)<<" function: "<<__PRETTY_FUNCTION__<<" file: "<<__FILE__<<" line: "<<__LINE__<<"\n"; abort(); }
}

#endif
