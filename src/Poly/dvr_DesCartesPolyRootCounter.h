
/** \file dvr_DesCartesPolyRootCounter.h
 * \brief Definition of the class dvr::DesCartesPolyRootCounter
 * \author Ramsharan Rangarajan
 * \date March 22, 2020. 
 */

#ifndef DVR_DESCARTES_POLY_ROOT_COUNTER_H
#define DVR_DESCARTES_POLY_ROOT_COUNTER_H

#include <cassert>
#include <cmath>
#include <iostream>

namespace dvr
{
  /** Service class for determining the number of real roots of a univariate
   * polynomial in a given interval in the real line using the Des cartes rule of signs.
   *
   * The class has no members; it is thread-safe.
   *  
   * \note This class could easily be replaced with a namespace.
   *
   * \todo Add functionality to count all real roots. This may not be useful in the current implemetation.
   */
  
  class DesCartesPolyRootCounter
  {
  public:
    //! Constructor, does nothing
    inline DesCartesPolyRootCounter() {}

    //! Destructor
    inline virtual ~DesCartesPolyRootCounter() {}

    //! Copy constructor
    //! \param[in] Obj Object from which to copy
    inline DesCartesPolyRootCounter(const DesCartesPolyRootCounter& Obj)
    {}

    //! Cloning
    inline virtual DesCartesPolyRootCounter* Clone() const
    { return new DesCartesPolyRootCounter(*this); }

    /** Method to count the number of real roots of a univariate polynomial
     * with given coefficients in a specified interval.
     *
     * \param[in] a Left endpoint of the interval
     * \param[in] b Right endppoint of the interval
     * \param[in] coef Coefficients of polynomial: coef[0] + coef[1] x + coef[2] x^2 + ..
     * \param[in] EPS Tolerance to use in deciding signs of coefficients
     * in looking for better estimates for roots.
     * \param[in] nsubdiv Number of interval subdivisions to perform if multiple roots are found in (a,b).
     * \tparam DEGREE Degree of the polynomial.
     * \return An upper bound on the number of real roots in (a,b)
     * \warning Does not account for the case in which one of the endpoints of the given
     * interval is already a root.
     * \note This function is not used by the dvr::Optimize routines. 
     * Only the dvr::DesCartesPolyRootCounter::QueryRoot method is used.
     */
     template<int DEGREE>
      int Count(const double& a, const double& b,
		const double* coef, const double EPS,
		int nsubdiv) const;
    

     /**  Queries if a univariate polynomial with given coefficients contains
      * a root in a given interval.
      *
      * The purpose and implementation of the method is very similar to Count(), 
      * except that a boolean answer needed here may be computable quicker
      * in some cases
      *      
      * \param[in] a Left endpoint of the interval
      * \param[in] b Right endppoint of the interval
      * \param[in] coef Coefficients of polynomial: coef[0] + coef[1] x + coef[2] x^2 + ..
      * \param[in] EPS Tolerance to use in deciding signs of coefficients
      * in looking for better estimates for roots.
      * \param[in] nsubdiv Number of interval subdivisions to perform  if multiple roots are found in (a,b).
      * \return True if a real root is possible in (a,b) and false otherwise
      * \warning Does not account for the case in which one of the endpoints of the given
      * interval is already a root.
      * \tparam DEGREE Degree of the polynomial.
      */
    template<int DEGREE>
      bool QueryRoot(const double& a, const double& b,
		     const double* coef, const double EPS,
		     int nsubdiv) const;
    
  private:
    //! Computes the mobius transform of a given polynomial
    //! \param[in] a Left endpoint of the interval
    //! \param[in] b Right endppoint of the interval
    //! \param[in] coef Coefficients of polynomial: coef[0] + coef[1] x + coef[2] x^2 + ..
    //! \param[out] P Coefficients of the transformed polynomial (x+1)^n P((ax + b)/(x+1)).
    //! \todo Simplify transformation coefficients, especially for higher degrees.
    template<int DEGREE>
      void Transform(const double& a, const double& b,
		     const double* coef, double* P) const;
    
    //! Count the number of sign changes in the coefficient list of a polynomial
    //! \param[in] DEGREE degree of polynomial
    //! \param[in] Coef Coefficients of polynomial
    //! \param[in] EPS Tolerance to use in checking sign of polynomial
    //! \param[in] flag SmallCoefFlag tracking if a small coefficient was found
    int CountSignChanges(const int DEGREE, const double* coef,
			 const double EPS, bool& SmallCoefFlag) const;    
  };
}

#endif

// Implementation
#include "./dvr_DesCartesPolyRootCounter_Impl.h"
