
/** \file dvr_GNUPolySolver_Impl.h
 * \brief Implementation of the class dvr::GNUPolySolver
 * \author Ramsharan Rangarajan
 * \date March 23, 2020. 
 */

#ifndef DVR_GNU_POLYSOLVER_IMPL_H
#define DVR_GNU_POLYSOLVER_IMPL_H

#include <cassert>
#include <src/dvr_assert.h>
#include <cmath>
#include <iostream>

namespace dvr
{
  // The cases MaxDegree > 3
  //==========================================
  
  // Constructor
  template<int MaxDegree>
    GNUPolySolver<MaxDegree>::GNUPolySolver()
    :Lambda(2*MaxDegree)
    { assert(MaxDegree>3);
      WorkSpcs.resize(MaxDegree-3);
      for( int i=0; i<MaxDegree-3; ++i)
	WorkSpcs[i] = gsl_poly_complex_workspace_alloc(i+5); }

  // Destructor
  template<int MaxDegree>
    GNUPolySolver<MaxDegree>::~GNUPolySolver()
    { for(auto& ws:WorkSpcs)
	gsl_poly_complex_workspace_free(ws); }

  // Copy constructor
  template<int MaxDegree>
    GNUPolySolver<MaxDegree>::GNUPolySolver(const GNUPolySolver<MaxDegree>& Obj)
    :Lambda(2*MaxDegree)
    { WorkSpcs.resize(MaxDegree-3);
      for( int i=0; i<MaxDegree-3; ++i)
	WorkSpcs[i] = gsl_poly_complex_workspace_alloc(i+5); }
  

  // Specialization of constructors + destructors for degree < 3
  //==========================================================

  // Disallow degree = 0
  //! Constructor specialization to disallow the case MaxDegree = 0
  template<> GNUPolySolver<0>::GNUPolySolver():Lambda{}
    { assert(false && "dvr::GNUPolySolver- Degree cannot be zero."); }
  
  // Degree = 1
  //! Constructor specialization for the case MaxDegree = 1. Workspace for solve is not allocated
  template<> GNUPolySolver<1>::GNUPolySolver() :Lambda(2) {}
  //! Destructor specialization for the case MaxDegree = 1
  template<> GNUPolySolver<1>::~GNUPolySolver() {}
  //! Copy constructor specialization for the case MaxDegree = 1
  template<> GNUPolySolver<1>::GNUPolySolver(const GNUPolySolver<1>& obj):Lambda(2) {}

  // Degree = 2
  //! Constructor specialization for the case MaxDegree = 2. Workspace for solve is not allocated.
  template<> GNUPolySolver<2>::GNUPolySolver() :Lambda(4) {}
  //! Destructor specialization for the case MaxDegree = 2
  template<> GNUPolySolver<2>::~GNUPolySolver() {}
  //! Copy constructor specialization for the case MaxDegree = 2
  template<> GNUPolySolver<2>::GNUPolySolver(const GNUPolySolver<2>& obj):Lambda(4) {}

  // Degree = 3
  //! Constructor specialization for the case MaxDegree = 3. Workspace for solve is not allocated
  template<> GNUPolySolver<3>::GNUPolySolver() :Lambda(6) {}
  //! Destructor specialization for the case MaxDegree = 2
  template<> GNUPolySolver<3>::~GNUPolySolver() {}
  //! Copy constructor specialization for the case MaxDegree = 2
  template<> GNUPolySolver<3>::GNUPolySolver(const GNUPolySolver<3>& obj):Lambda(6) {}

  //==========
  // Generic solver
  template< int MaxDegree>
    void GNUPolySolver<MaxDegree>::Solve(const int Degree, const double* coefs,
					 const double EPS, double* realroots,  int& nroots) const
    {
      // Check the degree
      assert(Degree<=MaxDegree && "GNUPolySolver::Solve()- Unexpected degree.");
      
      switch (Degree)
	{
	  // Constant
	case 0 : assert(false && "GNUPolySolver::Solve()- Degree = 0.");

	  // Linear
	case 1:
	  { realroots[0] = -coefs[0]/coefs[1];
	    nroots = 1; return; }
	
	  // Quadratic
	case 2:
	  { nroots = gsl_poly_solve_quadratic(coefs[2], coefs[1], coefs[0], &realroots[0], &realroots[1]);
	    return; }

	  // Cubic
	case 3:
	  { nroots = gsl_poly_solve_cubic(coefs[2]/coefs[3], coefs[1]/coefs[3], coefs[0]/coefs[3],
					  &realroots[0], &realroots[1], &realroots[2]);
	    return; }
	  
	  // Higher degree
	default:
	  {
	    auto success = gsl_poly_complex_solve(coefs, Degree+1, WorkSpcs[Degree-4], &Lambda[0]);
	    if(success!=GSL_SUCCESS)
	      {
		std::cout<<"Could not find root of polynomial with coefficients: ";
		for( int i=0; i<Degree+1; ++i)
		  std::cout<<coefs[i]<<", ";
		std::fflush( stdout );
		dvr_assert(false && "GNUPloySolver::Solve()- Could not solve.");
	      }
	    
	    // Identify real roots
	    nroots = 0;
	    double l1norm = 0.;
	    for( int i=0; i<Degree; ++i)
	      {
		l1norm = std::abs(Lambda[2*i])+std::abs(Lambda[2*i+1]);
		if( (l1norm<EPS) || std::abs(Lambda[2*i+1])/l1norm<EPS )
		  realroots[nroots++] = Lambda[2*i];
	      }
	    return;
	  }
	}
      return;
    }

}
#endif
