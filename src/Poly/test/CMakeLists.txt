
# Version of cmake tested with
cmake_minimum_required(VERSION 3.10.0)

# Project
project(testPoly)

# Set the default build type to "Debug"
if ("${CMAKE_BUILD_TYPE}" STREQUAL "")
      set(CMAKE_BUILD_TYPE "Debug")
endif()

# Check that the environment variable DVRDIR exists
if(NOT DEFINED ENV{DVRDIR})
	       message(FATAL_ERROR
				"Environment variable DVRDIR is not set")
endif()


# Find GNU GSL
find_package(GSL REQUIRED)


# Find OpenMP
find_package(OpenMP)
if(NOT OpenMP_CXX_FOUND)
 message(FATAL_ERROR
				"Could not locate OpenMP. Try using
				-DCMAKE_CXX_FLAGS=-fopemp" )
endif()


# Identical for all targets
foreach(TARGET
	    testDesCartes
	    testGNUPolySolver)

# Add this target
add_executable("${TARGET}"  "${TARGET}.cpp")

# Include directories
target_include_directories("${TARGET}" PUBLIC
			       		"$ENV{DVRDIR}")

# GSL
target_include_directories("${TARGET}" PUBLIC
			       		"${GSL_INCLUDE_DIRS}")					
target_link_libraries("${TARGET}" PUBLIC
		      	       GSL::gsl
		      	       GSL::gslcblas)

# OpenMP
target_link_libraries("${TARGET}" PUBLIC OpenMP::OpenMP_CXX)

# choose appropriate compiler flags
target_compile_features("${TARGET}" PUBLIC
			      		     cxx_auto_type
			      		     cxx_constexpr
			      		     cxx_static_assert
			      		     cxx_decltype
			      		     cxx_lambdas
			      		     cxx_nullptr)

endforeach()
