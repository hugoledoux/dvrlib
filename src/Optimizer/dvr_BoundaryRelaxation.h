
/** \file dvr_BoundaryRelaxation.h
 * \brief Definition of structs to help constrain nodes lying on a curvilinear boundary
 * \author Ramsharan Rangarajan
 * \date April 14, 2020
 */

#ifndef DVR_BOUNDARY_RELAXATION_H
#define DVR_BOUNDARY_RELAXATION_H

#include <src/Optimizer/dvr_RelaxationDirGenerators.h>

#ifdef _OPENMP
#include <omp.h>
#endif

namespace dvr
{
  using SignedDistanceFunction =
    std::function<void(const double* X, double& sd, double* dsd)>; /*!< Function type that evaluates the signed 
								    * distance function and its derivatives.
								    * \param[in] X Coordinates of the point where to evaluate the signed distance
								    * \param[out] sd Computed signed distance function \f$\phi(X)\f$. 
								    * \param[out] dsd Gradient of the signed distance function, \f$\nabla\phi(X)\f$. Should have unit norm.
								    */


  
  /** \brief Helper struct to encapsulate relaxation orthogonal to a level set function
   * The struct is templated by the spatial dimension and is thread-safe.
   * The tangential direction is computed as a (random) vector orthogonal to the gradient of the level set function.
   * \tparam SPD Spatial dimension. Should equal 2 or 3.
   * \ingroup dvr_relax_dirs
   * \see dvr::CartesianDirGenerator, dvr::RandomDirGenerator
   */
  template<int SPD>
    struct TangentialDirGenerator: public RelaxationDirGenerator
    {
    private:
      struct ParamStruct
      {
	SignedDistanceFunction SDfunc;
	std::random_device rd;
	std::vector<std::mt19937> genVec;
	std::vector<std::uniform_real_distribution<>> disVec;
      ParamStruct(SignedDistanceFunction sdfunc, int nthreads)
      :SDfunc(sdfunc),
	  genVec(nthreads, std::mt19937(rd())),
	  disVec(nthreads, std::uniform_real_distribution<>(-1.,1.)) {}
      };

    public:
      //! Constructor
      //! \param[in] sdfunc Signed distance function
      //! \param[in] nthreads Max number of threads. Defaulted to 1
      inline TangentialDirGenerator(SignedDistanceFunction sdfunc, const int nthreads=1)
	:dvr::RelaxationDirGenerator(),
	funcParams(sdfunc, nthreads)
	  {
	    assert(SPD==2 || SPD==3);
	    func = &TangentialDirection;
	    params = &funcParams;
	  }
      
      // Require template specializations
      inline static void TangentialDirection(const int vertnum, const double* X,
					     double* rdir, void* ctx)
      { assert(false); }

    private:
      ParamStruct funcParams; //!< Parameters to pass to the static method TangentialDirection
    };

 
  // Template specialization for tangential direction generator in 2D
  template<> void TangentialDirGenerator<2>::
    TangentialDirection(const int vertnum, const double* X,
			double* rdir, void* ctx)
    {
      // Access the signed distance function
      assert(ctx!=nullptr);
      const auto& pstruct = *static_cast<const ParamStruct*>(ctx);
      const auto& SDFunc = pstruct.SDfunc;
      
      // Compute the signed distance and its gradient
      double sd, dsd[2];
      SDFunc(X, sd, dsd);

      // Return the direction orthogonal to 'dsd'
      rdir[0] = dsd[1];
      rdir[1] = -dsd[0];

      // -- done --
      return;
    }

  // Tangential direction generator in 3D
  template<> void TangentialDirGenerator<3>::
    TangentialDirection(const int vertnum, const double* X,
			double* rdir, void* ctx)
    {
      const int SPD = 3;
      
      // Access the signed distance function, random number generators
      assert(ctx!=nullptr);
      auto& pstruct = *static_cast<ParamStruct*>(ctx);
      const auto& SDFunc = pstruct.SDfunc;
      int thrnum = 0;
#ifdef _OPENMP
      thrnum = omp_get_thread_num();
      assert(thrnum<static_cast<int>(pstruct.genVec.size()));
#endif
      auto& gen = pstruct.genVec[thrnum];
      auto& dis = pstruct.disVec[thrnum];

      // Signed distance
      double sd, dsd[SPD];
      SDFunc(X, sd, dsd);
      
      // Generate a random vector with a non-trivial component orthogonal to the normal
      while(true)
	{
	  double dot = 0.;
	  double norm = 0.;
	  for(int k=0; k<SPD; ++k)
	    {
	      rdir[k] = dis(gen);
	      norm += rdir[k]*rdir[k];
	      dot += rdir[k]*dsd[k];
	    }
	  norm = std::sqrt(norm);

	  // If norm is too small, regenerate the vector
	  if(norm<1.e-2) continue;

	  // Check projection along the normal
	  dot /= norm; // dot = 0 is ideal. dot = 1 is not.
	  if(std::abs(dot)>0.95) continue;
	  
	  // project rdir onto the tangent plane
	  for(int k=0; k<SPD; ++k)
	    { rdir[k] -= dot*dsd[k];
	      norm += rdir[k]*rdir[k]; }
	  norm = std::sqrt(norm);
	  for(int k=0; k<SPD; ++k)
	    rdir[k] /= norm;
	}
      
      // -- done --
      return;
    }



  
  /** \brief Helper struct that defines PointPerturbation as a closest point projection onto a domain boundary.
   * The struct is thread safe and is templated by the spatial dimension.
   * \tparam SPD Spatial dimension
   * \ingroup dvr_helpers
   */
  template<int SPD>
    struct ClosestPointStruct: public PointPerturbationStruct
    {
      //! Constructor
      inline ClosestPointStruct(SignedDistanceFunction sdfunc)
	:PointPerturbationStruct(),
	SDFunc(sdfunc)
	{ func = ClosestPointProjection;
	  params = &SDFunc; }

      inline static void ClosestPointProjection(double* X, void* ctx)
      {
	// Access the signed distance function
	assert(ctx!=nullptr);
	const auto& SDFunc = *static_cast<const SignedDistanceFunction*>(ctx);
      
	// Compute the signed distance and its gradient at X
	double sd, dsd[SPD];
	SDFunc(X, sd, dsd);
     
	// Closest point projection
	for(int k=0; k<SPD; ++k)
	  X[k] -= sd*dsd[k];

	// -- done --
	return;
      }

      SignedDistanceFunction SDFunc; //!< Function to compute signed distances and derivatives.
    };
}

#endif
