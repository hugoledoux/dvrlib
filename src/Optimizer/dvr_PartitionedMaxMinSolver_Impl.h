
/** \file dvr_PartitionedMaxMinSolver_Impl.h
 * \brief Implementation of the class dvr::PartitionedMaxMinSolver.
 * \author Ramsharan Rangarajan
 * \date March 22, 2020.
 */

#ifndef DVR_PARTITIONED_MAX_MIN_SOLVER_IMPL_H
#define DVR_PARTITIONED_MAX_MIN_SOLVER_IMPL_H

#include <cassert>
#include <src/dvr_assert.h>

#ifdef _OPENMP
#include <omp.h>
#endif

namespace dvr
{
  
  // Constructor
  template<typename QType> PartitionedMaxMinSolver<QType>::
    PartitionedMaxMinSolver(const QType& quality, const int nthreads)
    :Quality(quality),
    SolSpecs{.nMaxThreads=nthreads, 
      .nExtrema=quality.GetNumExtrema(),
      .nIntersections=quality.GetNumIntersections()},
    ThrPartData(SolSpecs.nMaxThreads)
      {
	// Check solver specs
	assert((SolSpecs.nMaxThreads>0 && SolSpecs.nExtrema>0 && SolSpecs.nIntersections>0) &&
	       "dvr::PartitionedMaxMinSolver: Unexpected solver specs.");

	// Size local data
	const auto& MD = Quality.GetMesh();
	const int ElmValency = MD.GetMaxElementValency();
	for(int thr=0; thr<SolSpecs.nMaxThreads; ++thr)
	  {
	    auto& data = ThrPartData[thr];

	    // Resize RingData structures
	    auto& ringdata = data.RD;
	    ringdata.LocalNodeNums.resize(ElmValency);
	    
	    // Each quality curve needs extremizers
	    auto& Extrema = data.Extrema;
	    Extrema.tcoord.resize(SolSpecs.nExtrema);
	    Extrema.qvals.resize(SolSpecs.nExtrema);
	    
	    // Data for pairs of intersections
	    auto& Intersections = data.Intersections;
	    Intersections.tcoord.resize( SolSpecs.nIntersections );
	    Intersections.qvals.resize( SolSpecs.nIntersections );
	  }
      }
  

  // Copy constructor
  template<typename QType> PartitionedMaxMinSolver<QType>::
    PartitionedMaxMinSolver(const PartitionedMaxMinSolver<QType>& Obj)
    :Quality(Obj.Quality),
    SolSpecs{.nMaxThreads=Obj.SolSpecs.nMaxThreads,
	.nExtrema=Obj.SolSpecs.nExtrema,
	.nIntersections=Obj.SolSpecs.nIntersections}
    {
      // resize local data
      const int nMaxThreads = SolSpecs.nMaxThreads;

      // Size local data
      const auto& MD = Quality.GetMesh();
      const int ElmValency = MD.GetMaxElementValency();
      for(int thr=0; thr<nMaxThreads; ++thr)
	{
	  auto& data = ThrPartData[thr];
	  
	  // resize RingData structures
	  auto& ringdata = data.RD;
	  ringdata.LocalNodeNums.resize(ElmValency);
	  
	  // Each quality curve needs extremizers
	  auto& Extrema = data.Extrema;
	  Extrema.tcoord.resize(SolSpecs.nExtrema);
	  Extrema.qvals.resize(SolSpecs.nExtrema);
	  
	  // Data for pairs of intersections
	  auto& Intersections = data.Intersections;
	  Intersections.tcoord.resize(SolSpecs.nIntersections);
	  Intersections.qvals.resize( SolSpecs.nIntersections);
	}
    }

  //! Provides thread-safe access to reconstruction-data
  template<typename QType> 
  detail::PartitionData& PartitionedMaxMinSolver<QType>::GetLocalData() const
    {
      int ThrNum = 0;
#ifdef _OPENMP
      ThrNum = omp_get_thread_num();
#endif
      assert(ThrNum<SolSpecs.nMaxThreads &&
	     "dvr::PartitionedMaxMinSolver::GetLocalData- exceeded max number of threads");
      return ThrPartData[ThrNum];
    }
  
  // Main functionality
  template<typename QType>
    double PartitionedMaxMinSolver<QType>::Solve(const int vert,  double* rdir,
						 void* usrparams) const
    {
      // Local reconstruction data
      auto& LocData = GetLocalData();

      // Access and populate 1-ring data structures
      auto& rd = LocData.RD;
      auto& n1RingElms = rd.n1RingElms;
      auto* My1RingElms = rd.My1RingElms;
      auto& LocalNodeNums = rd.LocalNodeNums;

      // 1-ring elements for this vertex
      const auto& MD = Quality.GetMesh();
      MD.Get1RingElements(vert, &My1RingElms, n1RingElms);
      dvr_assert((n1RingElms>0 && n1RingElms<=MD.GetMaxElementValency()) &&
	     "PartitionedMaxMinSolver::Solve:- Unexpected number of 1-ring elms.");
      dvr_assert(My1RingElms!=nullptr && "PartitionedMaxMinSolver::Solve- Could not access 1-ring elms");

      // Local node numbers in 1-ring elements
      const int NPE = MD.GetNumNodesPerElement();
      for( int i=0; i<n1RingElms; ++i)
	{
	  const auto* elmconn = MD.connectivity(My1RingElms[i]);
	  for(int a=0; a<NPE; ++a)
	    if(elmconn[a]==vert)
	      { LocalNodeNums[i] = a; break; }
	}

      // Optimal coordinate is the one where the minimum along all triangle qualities is maximal.
      double Qmin, qval;
      
      // Initialize optimal value to correspond to no perturbation
      double tOpt = 0.;
      Qmin = Quality.Compute(My1RingElms[0]);
      dvr_assert(Qmin>0. && "dvr::PartitionedMaxMinSolver::Solve- Initial quality is inadmissible (negative).");
      for(int i=1; i<n1RingElms; ++i)
	{
	  qval = Quality.Compute(My1RingElms[i]);
	  if(qval<Qmin) Qmin = qval;
	}
      double max_Qmin = Qmin;
      assert(max_Qmin>0. && "dvr::PartitionedMaxMinSolver::Solve- Initial quality is inadmissible (negative).");
      
      // Examine maximizers at element quality curves
      auto& tMax = LocData.Extrema.tcoord;
      auto& qMax = LocData.Extrema.qvals;
      auto& nMax = LocData.Extrema.nvals;
      double tval;
      for( int i=0; i<n1RingElms; ++i)
	{
	  const auto elm = My1RingElms[i];
	  const auto locnode = LocalNodeNums[i];
	  Quality.Extremize(elm, locnode, rdir, &tMax[0], &qMax[0], nMax);
	  dvr_assert(nMax>0 && "dvr::PartitionedMaxMinSolver::Solve- Could not find maxima for quality curve.");
	  
	  for( int r=0; r<nMax; ++r)
	    {
	      // Minimum quality realized at coordinate value = 'tval'
	      tval = tMax[r];
	      
	      // Initialize with first element
	      Qmin = Quality.Compute(My1RingElms[0], LocalNodeNums[0], rdir, tval);
	    
	      // Examine remaining elements
	      for( int j=1; j<n1RingElms; ++j)
		{
		  if(Qmin<max_Qmin) break; // Qmin cannot be an optimum
		  qval = Quality.Compute(My1RingElms[j], LocalNodeNums[j], rdir, tval);
		  if(qval<Qmin) Qmin = qval;
		}
	    
	      // Is tval a better candidate?
	      if(Qmin >= max_Qmin)
		{ max_Qmin = Qmin; tOpt = tval; }
	    }
	}
      
      
      // Examine pairwise intersections of element quality curves
      auto& tIntersect = LocData.Intersections.tcoord;
      auto& qIntersect = LocData.Intersections.qvals;
      auto& nIntersect = LocData.Intersections.nvals;
      for( int i=0; i<n1RingElms; ++i)
	{
	  const auto elmA = My1RingElms[i];
	  const auto locnodeA = LocalNodeNums[i];
	  for( int j=i+1; j<n1RingElms; ++j)
	    {
	      const auto elmB = My1RingElms[j];
	      const auto locnodeB = LocalNodeNums[j];
	      Quality.Intersect(elmA, locnodeA, elmB, locnodeB, rdir, &tIntersect[0], &qIntersect[0], nIntersect);

	      for( int r=0; r<nIntersect; ++r)
		{
		  // Compute the minimum quality realized at coordinate 'tval'
		  tval = tIntersect[r];
		
		  // Initialize with first element
		  Qmin = Quality.Compute(My1RingElms[0], LocalNodeNums[0], rdir, tval);
		  
		  // Examine remaining elements
		  for( int k=1; k<n1RingElms; ++k)
		    {
		      if(Qmin<max_Qmin) break; // Qmin cannot be an optimum
		      qval = Quality.Compute(My1RingElms[k], LocalNodeNums[k], rdir, tval);
		      if(qval<Qmin) Qmin = qval;
		    }

		  // Is tval a better candidate
		  if(Qmin >= max_Qmin)
		    { max_Qmin = Qmin; tOpt = tval; }
		}
	    }
	}
      return tOpt;
    }
}

#endif
