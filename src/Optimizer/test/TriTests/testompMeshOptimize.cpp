
/** \file testompMeshOptimize.cpp
 * \brief Example using dvr to optimize vertices in a triangle mesh in parallel.
 * \author Ramsharan Rangarajan
 * \date March 23, 2020
 *
 * Uses the mesh "superior.mesh" in tecplot format.
 * All interior nodes are relaxed. 
 * Computes a partitioning the relaxed set of nodes.
 * Optimizes vertices in the interior along Cartesian directions.
 * Prints the final mesh, and qualities of the initial and final meshes.
 */


#include <mesh/dvrmesh_SimpleMesh.h>
#include <src/Optimizer/dvr_MeshOptimizer.h>
#include <src/Optimizer/dvr_GeomTri2DQuality.h>
#include <src/Optimizer/dvr_PartitionedMaxMinSolver.h>
#include <src/Optimizer/dvr_RelaxationDirGenerators.h>
#include <src/Optimizer/dvr_VertexColoring.h>
#include <algorithm>
#include <cmath>
#include <cstdlib>
#ifdef _OPENMP
#include <omp.h>
#endif


using namespace dvr;
using namespace dvrmesh;

// Print the quality vector
template<class QType>
void PrintMeshQuality(const std::string& filename,
		      const std::vector<int>& Ir,
		      const QType& Quality);


int main()
{
  const std::string dvrdir = std::getenv("DVRDIR");
  assert(dvrdir!="");
  
  SimpleMesh MD;
  MD.nodes_element = 3;
  MD.spatial_dimension = 2;
  MD.ReadTecplotFile(dvrdir+"/mesh/superior.mesh");
  MD.SetupMesh();
  
  // Relax all nodes except the ones on the boundary
  const auto bdnodes = MD.GetBoundaryNodes();
  std::vector<int> allnodes(MD.nodes);
  for(int n=0; n<MD.nodes; ++n) allnodes[n] = n;
  std::vector<int> Ir{};
  std::set_difference(allnodes.begin(), allnodes.end(), bdnodes.begin(), bdnodes.end(),
		      std::inserter(Ir, Ir.begin()));
  
  // Set #threads for OPENMP
  const int nThreads = omp_get_max_threads()/2; // no virtual threads
  omp_set_num_threads(nThreads);
  
  // Create quality, specify #threads
  GeomTri2DQuality<decltype(MD)> Quality(MD, nThreads);
  
  // Max-min Solver
  PartitionedMaxMinSolver<decltype(Quality)> solver(Quality, nThreads);

  // Relaxation direction generator
  CartesianDirGenerator<2> dirGen;

  // Compute vertex coloring
  VertexColoring<decltype(MD)> vshades(MD, Ir);
  vshades.ConsistencyTest(); // optional
  
  // Print the mesh quality before relaxation
  PrintMeshQuality("q-pre.dat", Ir, Quality);
  
  // Iteratively relax
  const int nRelaxIters = 10;
  for(int iter=0; iter<nRelaxIters; ++iter)
    {
      // Alternate between Cartesian directions with iteration#
      dirGen.iteration = iter;
      Optimize(Ir, MD, solver, dirGen, vshades);
    }

  // Plot the mesh
  MD.PlotTecMesh("relaxed_mesh.tec");

  // Print quality of the relaxed mesh
  PrintMeshQuality("q-post.dat", Ir, Quality);
}


// Print the quality vector
template<class QType>
void PrintMeshQuality(const std::string& filename,
		      const std::vector<int>& Ir,
		      const QType& Quality)
{
  // Sorted vector of qualities at vertices
  const int nVerts = static_cast<int>(Ir.size());
  std::vector<double> qvec(nVerts);
  for(int i=0; i<nVerts; ++i)
    qvec[i] = ComputeVertexQuality(Ir[i], Quality);
  std::sort(qvec.begin(), qvec.end());

  // Print to file
  std::fstream qfile;
  qfile.open(filename.c_str(), std::ios::out);
  assert(qfile.good());
  qfile<<"# Index \t Vertex quality";
  for(int i=0; i<nVerts; ++i)
    qfile<<"\n"<<i+1<<" "<<qvec[i];  
  qfile.flush(); qfile.close();
  return;
}
