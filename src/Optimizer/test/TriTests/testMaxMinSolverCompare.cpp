
/** \file TriTests/testMaxMinSolverCompare.cpp
 * \brief Unit test for the classes dvr::ReconstructiveMaxMinSolver and dvr::PartitionedMaxMinSolver classes.
 * \author Ramsharan Rangarajan
 * \date March 23, 2020.
 *
 * The vertifies that vertex perturbations computed by the two classes for a mesh of triangles
 * are identical.
 *
 * Inputs: None
 *
 * Outputs: None if successful. Quits if any assertion checking correctness fails.
 */

#include <src/Optimizer/dvr_GeomTri2DQuality.h>
#include <mesh/dvrmesh_SimpleMesh.h>
#include <src/Optimizer/dvr_ReconstructiveMaxMinSolver.h>
#include <src/Optimizer/dvr_PartitionedMaxMinSolver.h>
#include <cmath>

using namespace dvrmesh;
using namespace dvr;

int main()
{
  // Five elements
  SimpleMesh MD;
  MD.nodes_element = 3;
  MD.spatial_dimension = 2;
  MD.Connectivity = std::vector<int>{0,1,2, 0,2,3, 0,3,4, 0,4,5, 0,5,1};
  MD.Coordinates = std::vector<double> {0.,0.,
	0.6*cos(M_PI/5.), 0.6*sin(M_PI/5.),
	1.2*cos(M_PI/2.), 1.2*sin(M_PI/2),
	0.5*cos(3*M_PI/4.), 0.5*sin(3*M_PI/4.),
	0.75*cos(6.*M_PI/5.), 0.75*sin(6.*M_PI/5.),
	1.25*cos(3.25*M_PI/2.), 1.25*sin(3.25*M_PI/2.)};
  MD.nodes = static_cast<int>(MD.Coordinates.size())/MD.spatial_dimension;
  MD.elements = static_cast<int>(MD.Connectivity.size())/MD.nodes_element;
  MD.SetupMesh();
  
  // Create quality
  GeomTri2DQuality<decltype(MD)> Quality(MD);
  
  // Solvers
  ReconstructiveMaxMinSolver<decltype(Quality)> RMMS(Quality);
  PartitionedMaxMinSolver<decltype(Quality)> PMMS(Quality);

  // Node being relaxed
  const int nodenum = 0;
  
  for(int iter=0; iter<360; ++iter)
    {
      // Perturbation direction
      double angle = M_PI*iter/(360.);
      double rdir[] = {cos(angle), sin(angle)};
      
      // Compute optimizers
      double lambda1 = RMMS.Solve(nodenum, rdir);
      double lambda2 = PMMS.Solve(nodenum, rdir);
      if(std::abs(lambda1-lambda2)>1.e-5)
	{
	  std::cout<<"\n"<<lambda1<<" did not equal "<<lambda2
		   <<" at iteration "<<iter; std::fflush( stdout );
	}
      assert(std::abs(lambda1-lambda2)<1.e-5 && "Optimizers did not match.");
    }
}
