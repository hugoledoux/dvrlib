
/** \file testVertexColoring.cpp
 * \brief Unit test for the class dvr::VertexColoring.
 * \author Ramsharan Rangarajan
 * \date March 23, 2020.
 *
 * Checks consistency of the coloring using an example of a triangle mesh.
 *
 * Inputs: none
 *
 * Outputs: none, if successful and an assertion otherwise.
 */

#include <src/Optimizer/dvr_VertexColoring.h>
#include <mesh/dvrmesh_SimpleMesh.h>
#include <random>
#include <algorithm>
#include <cstdlib>

using namespace dvrmesh;
using namespace dvr;

int main()
{
  const std::string dvrdir = std::getenv("DVRDIR");
  assert(dvrdir!="");
  
  SimpleMesh MD;
  MD.ReadTecplotFile(dvrdir+"/mesh/superior.mesh");
  MD.SetupMesh();
  
  // Color a random subset of vertices
  std::vector<int> Ir{};
  std::random_device rd;
  std::mt19937 gen(rd());
  std::bernoulli_distribution dis(0.8); // true 8/10 times
  for(int n=0; n<MD.nodes; ++n)
    if(dis(gen))
      Ir.push_back(n);
  std::shuffle(Ir.begin(), Ir.end(), gen);

  VertexColoring<decltype(MD)> vshades(MD, Ir);
  vshades.ConsistencyTest();
}
