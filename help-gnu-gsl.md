##### Test your GSL installation

Here is an example `toy.cpp` to help test your GSL installation:

```
#include <stdio.h>
#include <gsl/gsl_sf_bessel.h>
#include <iostream>

int main ()
{ std::cout<<"\n"<<gsl_sf_bessel_J0(5.0)<<"\n"; }

```
Successful execution should print -0.177597 (precision and format may vary).


** Compile and run from the terminal **  

* The destination directory used by GSL (eg `/usr/local/`) is usually
included in the default include and library paths. In such a case,
simply link the required libraries.  

```
g++ toy.cpp -o toy -lgsl -lgslcblas
./toy
```

* If you installed GSL in a custom location, then specify the
include and library paths:  
```
g++ toy.cpp -o toy -lgsl -lgslcblas -I/path_to_include -L/path_to_library
./toy
```

* To generate a makefile with CMake, use the`CMakeLists.txt` file:   
```
# Version of cmake
cmake_minimum_required(VERSION 3.10.0)

# Find GNU GSL
find_package(GSL REQUIRED)

# Add this target
add_executable(toy  toy.cpp)

# Set GSL include/library directories
target_include_directories(toy PUBLIC "${GSL_INCLUDE_DIRS}")
target_link_libraries(toy PUBLIC  GSL::gsl  GSL::gslcblas)

```

Generate a makefile:
```
mkdir build
cd build
cmake ../
make
./toy
```



