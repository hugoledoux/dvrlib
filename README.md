# DVRlib

DVRlib is a header-only C++ library providing an implementation of the directional vertex relaxation (DVR) algorithm to improve qualities of triangle and tetrahedral meshes.

## Features
* DVRlib achieves mesh improvement by optimally and iteratively perturbing locations of a specified set of vertices along prescribed directions.
* Element connectivities are retained unaltered during mesh improvement. In particular, the library does not rely on any topology-altering operations such as edge/face swapping and vertex insertion/removal.
* Despite relying solely on geometric mesh optimization, DVRlib can achieve significant improvement in mesh quality.
* The DVR algorithm implemented by DVRlib is provably robust. The mesh quality, defined in a specific sense, is guaranteed to improve monotonically with each vertex perturbation at each iteration.
* DVRlib provides vertex-level and mesh-level optimization routines.
* DVRlib provides a lock-free thread-parallel mesh optimization routine.
* DVRlib provides functionalities to relax vertices constrained to lie on curvilinear manifolds.
* DVRlib provides flexibility in the choice of mesh data structures. All classes and functionalities requiring access to meshes are templated by the mesh type.

## Dependencies
* [**GNU GSL library**](https://www.gnu.org/software/gsl/). 
* C++ compiler with support for C++11 features and OpenMP.
* Optional: [**CMake**](https://cmake.org) for generating makefiles to run tests and tutorial examples.
* Optional: [**Paraview**](https://www.paraview.org) for previewing mesh files in Tecplot format. 

## Installation
* DVRlib is a header-only library. No compilation or linking is required.
* Set the environment variable ``DVRDIR`` to the location of the home directory of DVRLIB (e.g., ``/User/name/code_repos/dvrlib/``). The source code uses include paths relative to ``$DVRDIR``.

## Start using DVRlib
To compile and run a tutorial-style example located in ``$DVRDIR/tutorial``:  

##### Using CMake
Using the ``CMakeLists.txt`` file provided to generate a makefile:  
```
    mkdir build                          # create a build directory in a convenient location  
    cd build  
    cmake $DVRDIR/tutorial/ .  # generate makefiles for the tutorial examples in the build directory  
    make                                   # creates 5 executables  
    ./dvr-triangle                         # or any of the other examples  
```  
To use a specific compiler (e.g. `CXX`):  
```
    cmake $DVRDIR/tutorial/ .  -DCMAKE_CXX_COMPILER=CXX
```  
To specify compiler flags (e.g. `-fopenmp`):  
```
    cmake $DVRDIR/tutorial/ .  -DCMAKE_CXX_COMPILER=CXX -DCMAKE_CXX_FLAGS="-fopenmp"
```
To specify the build type (e.g. Debug or  Release):  
```  
    cmake $DVRDIR/tutorial/ .  -DCMAKE_CXX_COMPILER=CXX -DCMAKE_BUILD_TYPE=Debug
```

##### Without CMake
Alternately, compile and link directly from the terminal:  
```
    mkdir build  
    cd build  
    CXX -std=c++11 $DVRDIR/tutorial/dvr-triangle.cpp -o dvr-triangle -I${DVRDIR} -I${GSL_INCLUDE_PATH} -L${GSL_LIBRARY_PATH} -lgsl -lgslcblas -fopenmp
```
where ``CXX`` is the C++ compiler (e.g., ``g++``), headers for the GSL library are located in ``${GSL_INCLUDE_PATH}`` and the libraries ``libgsl.a``, ``libgslcblas.a`` are assumed to be located in ``${GSL_LIBRARY_PATH}``.

*  `DVRlib` has been tested with the following CXX compilers:
   - GNU g++-mp-X (5, 6, 7, 8) compilers on macOS and Linux. 
   - Apple clang++ (8.0, 9.0) on macOS.
   - Intel icpc (2018) on Linux.

##### More information
* Installing GNU GSL:
	- Check [here](https://www.gnu.org/software/gsl/) for installation  instructions.
	- On Ubuntu (tested with 18.04): `sudo apt-get install  libgsl-dev`.
	- On macOS using [MacPorts](https://ports.macports.org/port/gsl/summary): `sudo port install gsl`. 

* *Trouble including/linking GNU GSL?* A toy example is provided
  [here](help-gnu-gsl.md) to test your installation.

* **Using OpenMP**
	- GNU's `libgomp` library is
    [here](https://gcc.gnu.org/onlinedocs/libgomp/). g++-mp-5 and
    compilers released later support OpenMP 4.0 using the `-fopenmp`
    flag.
	- To use OpenMP with Apple's `clang++`, you may need to install
      `libomp` (using
      [homebrew](https://formulae.brew.sh/formula/libomp) or
      [macports](https://ports.macports.org/port/libomp/summary)).

* *Trouble using OpenMP?* A toy example is provided
    [here](help-openmp.md) to test compiler compatibility and compiler
    flags.

##### Testing the library
* Detailed documentation for the library with discussions of tutorial-style examples is [**here**](https://rram.bitbucket.io/dvr-docs/html/index.html).
* A repository of meshes to test the library and reproduce benchmark results is [**here**](https://rram.bitbucket.io/dvr-meshes/index.html).  


## Intended applications
* DVRlib is well-suited for integration with unstructured mesh generators to improve element qualities through post-processing operations.
* DVRlib is designed to help simulate moving boundary problems, which require computing numerical solutions over domains that evolve with time (e.g., fluid-structure interaction) or domains that change with each iteration (e.g., shape optimization).
![Improving triangle and tetrahedral meshes with DVRlib](/paper/readme-fig.jpg)

## Projects using DVRlib
* Simulating mechanical contact as a shape optimization problem: *A Sharma, R Rangarajan. A shape optimization approach for simulating contact of elastic membranes with rigid obstacles.
International Journal for Numerical Methods in Engineering 117 (4), 371-404, 2019.*

* Meshing evolving 3D domains immersed in nonconforming tetrahedral meshes: *R Rangarajan, H Kabaria, A Lew. An algorithm for triangulating smooth three‐dimensional domains immersed in universal meshes. International Journal for Numerical Methods in Engineering 117 (1), 84-117, 2019.*

## More about the algorithms behind DVRlib
* For a detailed description and analysis of the DVR algorithm, see *Rangarajan R and Lew A. Provably robust directional vertex relaxation for geometric mesh optimization. SIAM Journal on Scientific Computing, 39(6):A2438–A2471, 2017* .
* For a description and analysis of the algorithm used to resolve non smooth max-min problems defining vertex perturbations in DVRlib, see *Rangarajan R. On the resolution of certain discrete univariate max–min problems. Computational Optimization and Applications, 68(1):163–192, 2017.* 

## Contributing to DVRlib Please check [here](CONTRIBUTING.md) for
details on reporting bugs.  The page also lists a few topics to
consider if you would like to contribute to the library.

## License

MIT License

Copyright (c) [2020] [Ramsharan Rangarajan]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
