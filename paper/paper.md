---
title: 'DVRlib: A C++ library for geometric mesh improvement using Directional Vertex Relaxation'
tags:
  - C++
  - unstructured meshes
  - optimization
  - mesh improvement
  - finite element
  - moving boundary problems
authors:
  - name: Ramsharan Rangarajan
    affiliation: 1
  - name: Adrian Lew
    affiliation: 2
affiliations:
  - name:  Department of Mechanical Engineering, Indian Institute of Science Bangalore, India
    index:  1
  - name:  Department of Mechanical Engineering, Stanford University, USA
    index:  2
date: 24 June 2020
bibliography: paper.bib
---

# Summary

The success of finite element methods in simulating models arising in
physical sciences, applied mathematics, and computer graphics relies
on generating high-quality unstructured meshes.  In general,
well-shaped elements are essential to ensure accurate numerical
approximations, to improve the conditioning of systems of equations to
be resolved, and to choose reasonable step sizes in time integration
schemes.

`DVRlib` is a C++ library that implements the directional vertex
relaxation algorithm introduced in @rangarajan2017provably to improve
qualities of unstructured meshes. `DVRlib` improves qualities of
triangular and tetrahedral elements by *iteratively* and *optimally*
perturbing a selected set of vertices along prescribed directions.
During the process, the connectivity of the mesh remains unaltered;
just the locations of vertices may change. `DVRlib` is straightforward
to couple with existing libraries for unstructured mesh generation by
invoking the functionalities it provides as a simple post-processing
step.  Equally significantly, `DVRlib` enables engineers and
researchers to simulate a challenging class of moving boundary
problems by helping to maintain good element qualities in deforming
meshes.

# What `DVRlib` does (and doesn't)

`DVRlib` provides robust and efficient functionalities for improving
element qualities in triangular and tetrahedral meshes by perturbing
locations of a specified set of vertices along a prescribed set of
directions. Despite relying solely on vertex perturbations for
(geometric) mesh relaxation, `DVRlib` generally achieves significant
improvement in mesh quality. The documentation accompanying the
library provides numerous examples demonstrating improvement in mesh
quality achieved using `DVRlib` when relaxing meshes produced by
commonly used mesh generators
[@si2015tetgen; @geuzaine2009gmsh; @shewchuk1996triangle; @hypermesh; @cgal:rty-m3-20a].

A large number of software libraries provide a variety of tools for
generating, improving and manipulating unstructured meshes
[@si2015tetgen; @geuzaine2009gmsh; @shewchuk1996triangle; @cgal:eb-20a; @netgen; @hypermesh].
In this context, it is important to note what `DVRlib` is *not*:  
* `DVRlib` is not a mesh generator but can be coupled to one.
* `DVRlib` is not a tool for adaptive mesh refinement but can improve an adaptively refined mesh.
* `DVRlib` is not a mesh untangling tool but can improve an untangled
  mesh.
* `DVRlib` is not a mesh simplification tool.

![Examples illustrating connectivity-preserving mesh improvement with DVRlib.\label{fig:1}](fig1.jpg)

`DVRlib` is neither intended to be nor is a comprehensive mesh
improvement toolbox. `DVRlib` performs a very specific type of vertex
relaxation to improve mesh qualities. Nevertheless, `DVRlib` can be
coupled with libraries providing a complementary suite of operations,
including topological operations such as edge/face swapping, vertex
insertion/removal and local remeshing
[@optimesh; @mesquite; @vartziotis2018getme; @cgal:eb-20a].


# Statement of need

Despite the large volume of literature on the topic and the extensive
arsenal of operations available for mesh relaxation, mesh improvement
remains a challenging problem. The state of affairs is well summarized
in the words of @geuzaine2009gmsh: *“mesh optimization procedures have
a lot to do with ‘black magic’: even if the ingredients required to
construct a mesh optimization procedure are well known (essentially
swapping and smoothing), there is no known ‘best recipe’, i.e. no
known optimal way of combining those smoothing and swapping
operators.”*


**Mesh relaxation with guarantees.** The directional vertex relaxation
(DVR) algorithm implemented by `DVRlib` enjoys the distinctive feature
of being provably robust. DVR guarantees that the mesh quality, and in
particular, the poorest quality among elements being perturbed
improves monotonically with each vertex perturbation at each iteration
[@rangarajan2017provably]. These guarantees hold independently of the
choice of vertices being relaxed and the choice of relaxation
directions, and even when computing with finite precision.  These
features of DVR, which are inherited by its implementation in
`DVRlib`, are notable when compared with commonly used mesh smoothing
techniques that are often based on heuristic analogies with mechanical
systems [@persson2004simple; @degand2002three]. Furthermore, unlike
mesh relaxation algorithms that require resolving systems of equations
[@freitag2002tetrahedral; @budd2009adaptivity], `DVRlib` relaxes
vertices by resolving single-variable optimization problems.


**Simulating moving boundary problems.** Simulating problems involving
domains that evolve with time present algorithmic challenges stemming
from deterioration in mesh quality due to element
distortion. Fluid-structure interaction and shape optimization
problems, which are of a broad interest in research and engineering,
fall in this category.  Practical simulation strategies invariably
rely on remeshing the domain once the mesh is deemed to be
sufficiently distorted [@mmgs]. The functionalities provided by
`DVRlib` are well-suited to aid such simulations, see \autoref{fig:1}.
First, a deforming mesh can be relaxed periodically with `DVRlib` and
hence used for longer durations (either in time or number of
iterations) before remeshing entirely.  Second, since `DVRlib` retains
element connectivities during mesh relaxation, the sparsity of data
structures involved in a simulation are preserved (e.g., mass and
stiffness matrices) so that expensive memory reallocation is avoided.
Third, the guarantee of robustness underlying the mesh relaxation
algorithm implemented by `DVRlib` is crucial when simulating moving
boundary problems, since user-interactivity in guiding mesh
improvement at each time step or each iteration is impossible.

In this context, we mention that `DVRlib` is an integral component of
on-going research on universal meshes for discretizing evolving
domains [@rangarajan2019algorithm]. We refer to @sharma2019shape for
an application that uses `DVRlib` to adapt triangle meshes to conform
to moving contact interfaces that arise when simulating elastic
contact.


**Non-smooth max-min optimization.** The mathematical problem defining
vertex perturbations in the DVR algorithm is of the form:
\begin{equation} \text{Find}~\lambda^{\rm opt} \in
\arg\max_{\lambda\in {\mathbb R}} {\rm F}(\lambda),\quad \text{where}~
{\rm F}(\lambda) \triangleq
\min\{f_1(\lambda),f_2(\lambda),\ldots,f_n(\lambda)\}.  \label{eq:opt}
\end{equation} Each univariate scalar-valued function $\lambda\mapsto
f_\alpha(\lambda)$ appearing in (\ref{eq:opt}) represents the
variation in quality of an element as one of its nodes is perturbed
along a prescribed direction by the coordinate $\lambda\in {\mathbb
R}$. The vertex perturbation $\lambda^{\rm opt}$ is optimal in the
sense that it maximizes the minimum among the qualities of all
elements incident at the vertex.

Computing $\lambda^{\rm opt}$, however, is a challenge.  Since element
quality metrics generally depend smoothly on vertex locations, each
function $f_\alpha$ is smooth.  As the minimum among the functions
$\{f_\alpha\}_\alpha$, however, ${\rm F}$ is continuous but not
differentiable. Hence, eq. (\ref{eq:opt}) represents a non-smooth
max-min optimization problem for which straightforward Newton-type
methods do not apply. Ad hoc techniques are ill-advised because mesh
relaxation requires resolving one such problem for each vertex
perturbation at each iteration of the algorithm.  Heuristic methods
are also likely to be inefficient, since the number of functions $n$,
which equals the valence of the vertex being relaxed, can be large. In
tetrahedral meshes, for example, $n$ ranges from $25$ to $30$ for
commonly used mesh generators. One of the main functionalities
provided by `DVRlib` is an efficient implementation of the algorithm
introduced and analyzed in @rangarajan2017resolution to resolve
(\ref{eq:opt}) for a specific class of element quality metrics.

![Performance benchmarks for serial and thread-parallel mesh optimization routines in `DVRlib`.\label{fig:2}](fig2.jpg)

# Functionalities in `DVRlib`

* *Mean ratio element quality metrics* for 2D triangles and 3D
  tetrahedra.
* *Max-min solvers* to compute optimal unconstrained vertex
 perturbations and sampling-based suboptimal constrained vertex
 perturbations.
* *Relaxation direction generators* for vertex relaxation along
Cartesian and random directions, and along tangent planes to
curvilinear manifolds.
* *Mesh optimization routines.* Overloaded vertex and mesh
optimization routines for serial and thread-parallel execution, see
\autoref{fig:2}.
* *Mesh data structures.* Recognizing the diversity in commonly used
mesh data structures, `DVRlib` retains the mesh type as a template
parameter throughout. This also facilitates integrating `DVRlib` into
high-performance computing codes. A rudimentary mesh data structure is
included with the library for testing.

The documentation for `DVRlib` provides a detailed set of
tutorial-style examples for users to get started. The library also
provides a mesh repository to help test its functionalities and to
reproduce the benchmark examples discussed in the documentation.

We expect future extensions of the library to introduce new mesh types
(e.g., quadrilaterals, bricks) and to incorporate a broader class of
element quality metrics.

# Acknowledgements
RR thanks the Defence Research and Development Organization's (DRDO, India) research grant 
JATP/P-VIII/P-2019/164 through the Joint Advanced Technology Program with the Indian Institute of Science Bangalore, 
the Science and Engineering Research Board's (SERB, India) early career award ECR/2017/000346, 
and Ms. Varshini Subash for assistance with code profiling and testing as part of her undergraduate thesis.

# References
