
/** \file dvrmesh_SimpleMesh_Impl.h
 * \brief Implementation of the class SimpleMesh.
 * \author Ramsharan Rangarajan
 * \date March 23, 2020.
 */

#ifndef DVRMESH_SIMPLE_MESH_IMPL_H
#define DVRMESH_SIMPLE_MESH_IMPL_H

#include <src/dvr_assert.h>
#include <fstream>
#include <unordered_set>
#include <algorithm>

namespace dvrmesh
{
  // Compute Vertex 1-rings
  void SimpleMesh::SetupMesh()
  {
    // Sanity checks
    assert( (nodes_element==3 && spatial_dimension==2) || (nodes_element==4 && spatial_dimension==3));
    assert(static_cast<int>(Connectivity.size())==elements*nodes_element);
    assert(static_cast<int>(Coordinates.size())==spatial_dimension*nodes);

    // Guess for sizing vectors
    const int valencyGuess = (spatial_dimension==2) ? 12 : 30; // guesses for reserving memory
    Vert1Rings.resize(nodes);
    Elm1Rings.resize(nodes);
    for(int n=0; n<nodes; ++n)
      { Vert1Rings[n].reserve(valencyGuess);
	Elm1Rings[n].reserve(valencyGuess); }
    
    // Loop over connectivites and update 1-ring information
    for(int e=0; e<elements; ++e)
      {
	const auto* elmconn = connectivity(e);
	for(int a=0; a<nodes_element; ++a)
	  {
	    const int& vert = elmconn[a];
	    Elm1Rings[vert].push_back(e);
	    for(int k=1; k<nodes_element; ++k)
	      {
		const int& nbvert = elmconn[(a+k)%nodes_element];
		Vert1Rings[vert].push_back( nbvert );
	      }
	  }
      }
    
    // Shink sizes
    for(auto& it:Vert1Rings) it.shrink_to_fit();
    for(auto& it:Elm1Rings) it.shrink_to_fit();

    // Maximum vertex valency
    nMaxVertValency = 0;
    for(auto& it:Vert1Rings)
      { const int nverts = static_cast<int>(it.size());
	if(nMaxVertValency<nverts) nMaxVertValency = nverts; }

    // Maximum element valency
    nMaxElmValency = 0;
    for(auto& it:Elm1Rings)
      { const int nElms = static_cast<int>(it.size());
	if(nMaxElmValency<nElms) nMaxElmValency = nElms; }

    // Update the state of the mesh
    isFinalized = true;
    
    // done
    return;
  }


  // Read from a file in tecplot format
  void SimpleMesh::ReadTecplotFile(const std::string filename)
  {
    // lambda to positions cursor to get the data after 'word' occurs
    auto PositionCursor = [](std::ifstream &ifile, const std::string word)
      { ifile.seekg(0, std::ios_base::beg);
	while(ifile.good())
	  { if(ifile.get() == word[0])
	      { unsigned int ipos=1;
		while(ipos!= word.length())
		  if(ifile.get() == word[ipos]) ipos++;
		  else  break;
		if(ipos == word.length())
		  return true; } }
	return false;  };

    // Open the given file
    std::ifstream TecFile;
    TecFile.open(filename, std::ifstream::in);
    dvr_assert(TecFile.good() && "ReadTecplotFile- Could not open file");

    // Read  number of nodes:
    dvr_assert(PositionCursor(TecFile, std::string("N=")) &&
	       "ReadTecplotFile- Could not read number of nodes");
    TecFile >> nodes;
  
    // Read number of elements:
    dvr_assert(PositionCursor(TecFile, std::string("E=")) &&
	       "ReadTecplotFile- Could not read number of elements");
    TecFile >> elements;
  
    // Read Element type
    std::string ET;
    dvr_assert(PositionCursor(TecFile, std::string("ET=")) &&
	       "ReadTecplotFile- Could not read element type");
    TecFile>>ET;
    std::transform(ET.begin(), ET.end(), ET.begin(), ::toupper);
    dvr_assert(ET=="TRIANGLE" || ET=="TETRAHEDRON");
    if(ET=="TRIANGLE")
      { spatial_dimension = 2;
	nodes_element = 3; }
    else
      { spatial_dimension = 3;
	nodes_element = 4; }

    // Resize coordinates and connectivity vectors
    Coordinates.clear();
    Connectivity.clear();
    Coordinates.resize(nodes*spatial_dimension);
    Connectivity.resize(elements*nodes_element);

    // Read coordinates
    for(int a=0; a<nodes; a++)
      for(int k=0; k<spatial_dimension; ++k)
	TecFile>>Coordinates[spatial_dimension*a+k];

    // Read connectivity
    for(int e=0; e<nodes_element*elements; ++e)
      { TecFile>>Connectivity[e]; --Connectivity[e]; } 
    TecFile.close();

    // done
    return;
  }


  // Plot a mesh in tecplot format
  void SimpleMesh::PlotTecMesh(const std::string filename) const
  {
    // Open file to plot
    std::fstream outfile;
    outfile.open(filename, std::ios::out);
    outfile.precision(16);
    outfile.setf( std::ios::scientific );
  
    // Line 1:
    if( spatial_dimension==2 )
      outfile<<"VARIABLES = \"X\", \"Y\" ";
    else 
      outfile<<"VARIABLES = \"X\", \"Y\", \"Z\" ";
    outfile<<"\n";

    // Line 2:
    const std::string ElmType = (spatial_dimension==2)? "TRIANGLE" : "TETRAHEDRON";
    outfile<<"ZONE t=\"t:0\", N="<<nodes
	   <<", E="<<elements
	   <<", F=FEPOINT, ET="<<ElmType;
  
    // Nodal coordinates
    for(int a=0; a<nodes; ++a)
      {
	const auto* X = coordinates(a);
	outfile<<"\n";
	for(int k=0; k<spatial_dimension; ++k)
	  outfile<<X[k]<<" ";
      }
  
    // Element connectivity
    for(int e=0; e<elements; ++e)
      {
	outfile<<"\n";
	const auto* conn = connectivity(e);
	for(int a=0; a<nodes_element; ++a)
	  outfile<<conn[a]+1<<" ";
      }
    outfile.flush();
    outfile.close();
  }


  // Identify all nodes lying on the boundary of a mesh
  std::set<int> SimpleMesh::GetBoundaryNodes() const
    {
      // Enumerate faces by sorted connectivities
      // If a duplicate face is found, erase it
      std::set<std::set<int>> freeFaces({});
  
      // Local numbering of faces
      const std::vector<int> locFaceNodes = (nodes_element==3) ?
	std::vector<int>{0,1, 1,2, 2,0} :             // Face numbering for triangles
	std::vector<int>{2,1,0, 2,0,3, 2,3,1, 0,1,3}; // Face numbering for tetrahedra

      // # faces per element, # nodes per face
      const int nFacesPerElm = nodes_element;
      const int nNodesPerFace = nodes_element-1;
  
      for(int e=0; e<elements; ++e)
	{
	  const int* conn = connectivity(e);
	  for(int f=0; f<nFacesPerElm; ++f)
	    {
	      // Sorted connectivity of this face
	      std::set<int> fconn({});
	      for(int a=0; a<nNodesPerFace; ++a)
		fconn.insert(conn[locFaceNodes[nNodesPerFace*f+a]]);

	      // Does it exist in the list
	      auto it = freeFaces.find(fconn);
	      if(it!=freeFaces.end())
		freeFaces.erase(it); // This is a duplicate
	      else
		freeFaces.insert(fconn); // Not a duplicate
	    }
	}
	
      // Accummulate the list of nodes along the boundary
      std::set<int> bdnodes({});
      for(auto& fconn:freeFaces)
	for(auto& n:fconn)
	  bdnodes.insert(n); 

      // done
      return std::move(bdnodes);
    }
}
#endif
