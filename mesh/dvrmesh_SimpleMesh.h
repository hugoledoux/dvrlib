
/** \file dvrmesh_SimpleMesh.h
 * \brief Definition of the class dvrmesh::SimpleMesh.
 * \author Ramsharan Rangarajan
 * \date March 23, 2020.
 */

#ifndef DVRMESH_SIMPLE_MESH_H
#define DVRMESH_SIMPLE_MESH_H

#include <vector>
#include <string>
#include <set>
#include <cassert>

//! Namespace for an ad-hoc mesh class.
namespace dvrmesh
{
  /** \brief Class implementing a rudimentary mesh data structure.
   * The class provides the following methods which are assumed in the dvr library:
   * \code{.cpp}
   * 
   * // Method providing access to nodal coordinates
   *  const double* coordinates(const int& node_index) const; 
   *
   * // Method to update nodal coordinates
   * void update(const int node_index, const double* X); 
   *
   *  // Method providing access to element connectivities
   *  const int* connectivity(const int elm_index) const; 
   * 
   *  // Method to return the number of nodes per element
   *  int GetNumNodesPerElement() const;
   *
   *  // Method to return the spatial dimension in which the mesh is embedded
   *  int GetSpatialDimension() const; 
   *
   *  // Method that returns the list of vertices in the 1-ring.
   *  void Get1RingVertices(const int vert_index, const int** oneRingVerts, int& nVerts) const; 
   *
   *  // Method that returns the list of elements in the one ring of a vertex being relaxed
   *  void Get1RingElements(const int vert_index, const int** oneRingElms, int& nElms) const;
   *
   *  // Method that returns an upper bound on the number of vertices in 1-rings of vertices in Ir.
   *  int GetMaxVertexValency() const;
   *
   *  // Method that returns an upper bound on the number of elements in 1-rings of vertices in Ir.
   * int GetMaxVertexValency() const;
   *
   * \endcode
   *
   * The class is very rudimentary:
   * <ul>
   * <li> Nodes are numbered sequentially, starting from 0 to SimpleMesh::nodes-1. </li>
   * <li> Elements are numbered sequentially, starting from 0 to SimpleMesh::elements-1. </li>
   * <li> Nodal coordinates are stored sequentially in the public vector SimpleMesh::Coordinates.
   *  Coordinates of the node <tt>n</tt> is located at SimpleMesh::Coordinates[SPD*<tt>n</tt>],
   * where SPD equals the embedding dimension SimpleMesh::spatial_dimension. </li>
   * <li> Element connectivities are stored sequentially in the public vector SimpleMesh::Connectivity.
   * Connectivity of the element <tt>e</tt> is located at SimpleMesh::Connectivity[nodes_element*e],
   * where SimpleMesh::nodes_element equals 3 for a planar triangle and 4 for a tetrahdron. </li>
   * <li> The class should be used only with planar triangle and three-dimensional tetrahedral meshes. </li>
   * </ul>
   *
   * In addition to the methods required in the library (mentioned above), the class
   * provides a additional utilities:
   * <ul>
   * <li> To read a mesh from a tecplot file, through the SimpleMesh::ReadTecplotFile method. </li>
   * <li> To print a mesh in tecplot file, through the SimpleMesh::PlotTecFile method. </li>
   * <li> To identify nodes lying on the boundary of the mesh, through the SimpleMesh::GetBoundaryNodes method. </li>
   * </ul>
   *
   * To calculation of vertex 1-rings (vertices and elements) requires invoking the SimpleMesh::SetRelaxedVertices.
   * This is required before invoking the methods SimpleMesh::Get1RingVertices and SimpleMesh::Get1RingElements.
   *
   * This class is provided solely for the purpose of testing and examples.
   * <br>
   * It is neither carefully designed nor particularly efficient.
   * <br>
   * The class can be easily abused-- for example, it is possible to
   * add/delete nodes/elements through public access to the list of nodal coordinates and element connectivities,
   * which will invalidate all 1-ring vertex/element lists.
   * While useful for prototyping, SimpleMesh is not expected to be an integral part of production codes.
   *
   */
  class SimpleMesh
  {
  public:
    //! Constructor
    inline SimpleMesh()
      :Coordinates(),
      Connectivity({}),
      spatial_dimension(0),
      nodes_element(0),
      nodes(0), elements(0),
      isFinalized(false),
      Vert1Rings({}),
      Elm1Rings({}),
      nMaxVertValency(0),
      nMaxElmValency(0)
      	{}

    //! Destructor
    inline virtual ~SimpleMesh() {}

    //! Disable the copy constructor
    SimpleMesh(const SimpleMesh&) = delete;

    //! Disable assignment
    SimpleMesh& operator=(const SimpleMesh&) = delete;
  
    //! Returns the coordinates of a specified node
    //! \param[in] node Node number
    //! \return Coordinates of the requested node
    inline const double* coordinates(const int node) const 
    { return &Coordinates[spatial_dimension*node]; }

    //! Overwrites nodal coordinates
    //! Hence node number should equal the current number of nodes.
    //! \param[in] node Node number
    //! \param[in] X Coordinates
    inline void update(const int node, const double* X) 
    {
      assert(node<nodes);
      for(int k=0; k<spatial_dimension; ++k)
	Coordinates[spatial_dimension*node+k] = X[k];
    }
  
    //! Returns the connectivity of a specified element
    //! \param[in] elm Element number
    //! \return Connectivity of the requested element.
    inline const int* connectivity(const int elm) const 
    { return &Connectivity[nodes_element*elm]; }
      
    //! Returns the number of elements
    //! \return Total number of elements in the mesh
    inline int GetNumElements() const 
    { return elements; }
  
    //! Returns the number of nodes per element.
    //! Assumed to be the same for all elements.
    //! \return Number of nodes an any element.
    inline  int GetNumNodesPerElement() const 
    { return nodes_element; }

    //! Returns the spatial dimension of the mesh
    inline int GetSpatialDimension() const
    { return spatial_dimension; }

    //! Finalize the state of the mesh and compute 1-rings and vertex valencies for the mesh
    void SetupMesh();
  
    //! Returns the list of vertices in the 1-ring of a vertex
    //! \param[in] vert_index Vertex number. Should be a vertex in Ir
    //! \param[out] oneRingVerts Pointer to list of vertices in the 1-ring
    //! \param[out] nVerts Number of vertices in the 1-ring
    inline void Get1RingVertices(const int vert_index,
				 const int** oneRingVerts, int& nVerts) const
    {
      assert(isFinalized==true && "dvrmesh::SimpleMesh::Get1RingVertices- Call SetupMesh()");
      assert(vert_index>=0 && vert_index<nodes);
      *oneRingVerts = &Vert1Rings[vert_index][0];
      nVerts = static_cast<int>(Vert1Rings[vert_index].size());
      return;
    }

    //! Returns the list of elements in the 1-ring of a vertex
    //! \param[in] vert_index Vertex number. Should be a vertex in Ir
    //! \param[out] oneRingElms Pointer to list of elements in the 1-ring
    //! \param[out] nElms Number of elements in the 1-ring
    inline void Get1RingElements(const int vert_index,
				 const int** oneRingElms, int& nElms) const
    {
      assert(isFinalized==true && "dvrmesh::SimpleMesh::Get1RingElements- Call SetupMesh()");
      assert(vert_index>=0 && vert_index<nodes);
      *oneRingElms = &Elm1Rings[vert_index][0];
      nElms = static_cast<int>(Elm1Rings[vert_index].size());
      return;
    }
    
    //! Returns the maximum vertex valency
    inline int GetMaxVertexValency() const
    { assert(isFinalized==true && "dvrmesh::SimpleMesh::GetMaxVertexValency- Call SetupMesh()");
      return nMaxVertValency; }

    //! Returns the maximum element valency
    inline int GetMaxElementValency() const
    { assert(isFinalized==true && "dvrmesh::SimpleMesh::GetMaxElementValency- Call SetupMesh()");
      return nMaxElmValency; }

    //! \brief Method to initialize the object by reading from a tecplot file
    //! \param[in] filename File to be read
    void ReadTecplotFile(const std::string filename);

    //! \brief Print the mesh to a file in tecplot format
    //! \param[in] filename Filename under which to plot
    void PlotTecMesh(const std::string filename) const;

    //! \brief Method to identify all nodes lying on the boundary of a mesh
    //! \return Set of boundary nodes (by value)
    std::set<int> GetBoundaryNodes() const;

    // Members
    std::vector<double> Coordinates; //!< Vector of coordinates
    std::vector<int> Connectivity; //!< Vector of connectivity
    int spatial_dimension; //!< Spatial dimension
    int nodes_element; //!< Number of nodes per element
    int nodes; //!< Number of nodes in the mesh
    int elements; //!< Number of elements in the mesh

  protected:
    bool isFinalized; //!< Indicates the state of the mesh.
    std::vector<std::vector<int>> Vert1Rings; //! Vert1Rings[indx] = List of vertices in the 1-ring of vertex indx
    std::vector<std::vector<int>> Elm1Rings;  //! Elm1Rings[indx] = List of elements in the 1-ring of element indx
    int nMaxVertValency; //! maximum vertex valency in a 1-ring
    int nMaxElmValency;  //! maximum element valency in a 1-ring
  };
}

#endif


// Implementation of the class
#include "./dvrmesh_SimpleMesh_Impl.h"
